Useful shorthands or mnemonics for built-in commands.
These aliases should be self contained and not rely on any other files.

#!/bin/sh
# https://stackoverflow.com/a/152741
find ./sound -depth -exec rename 's/(.*)\/([^\/]*\.mp3)/$1\/\L$2/' {} \;
